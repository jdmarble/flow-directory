==============
Flow Directory
==============

.. image:: screenshot.png

Serves a web directory of services running on a Docker Swarm.
Meant to run along side Docker Flow Proxy.

The contents of the web page are generated by querying the Swarm manager
for services with specific labels.

========================================  ===========================  ===================================================================
 Label                                     Description                    Example value
========================================  ===========================  ===================================================================
pw.jdmarble.flow_directory                Must be present                "true"
pw.jdmarble.flow_directory.main           Use only for flow-directory    "true"
pw.jdmarble.flow_directory.display_name   Title of service               "Jenkins"
pw.jdmarble.flow_directory.icon           Trendy image                   "https://example.com/Jenkins_logo.png"
pw.jdmarble.flow_directory.documentation  Text explaining the service    "Jenkins manages automated software builds accross the build farm."
pw.jdmarble.flow_directory.servicePath    Relative link                  "/jenkins"
com.df.servicePath                        Alternative                    "/jenkins"
========================================  ===========================  ===================================================================

Run Flow Directory as a service in the swarm::

    version: '3'

    services:
      directory:
        image: registry.gitlab.com/jdmarble/flow-directory
        volumes:
          - "/var/run/docker.sock:/var/run/docker.sock"
        networks:
          - proxy_proxy
        deploy:
          labels:
            com.df.notify: "true"
            com.df.distribute: "true"
            com.df.servicePath: "/"
            com.df.port: 80
            pw.jdmarble.flow_directory: "true"
            pw.jdmarble.flow_directory.main: "true"
            pw.jdmarble.flow_directory.display_name: "My Swarm Name"
            pw.jdmarble.flow_directory.documentation: >
                    This is a list of services running on My Swarm.
          placement:
            constraints:
              - node.role == manager

    networks:
      proxy_proxy:
        external: true

