from flask import Flask, render_template
import docker

label_prefix = "pw.jdmarble.flow_directory"
docker_flow_prefix = "com.df"
app = Flask(__name__)
client = docker.from_env()


def filter_prefix_labels(labels, prefix):
    return {
        k[len(prefix + "."):]: v
        for k, v in labels.items()
        if k.startswith(prefix)
    }


@app.route('/')
def index():

    services = []
    directory_service = None
    for service in client.services.list(filters=dict(label=label_prefix)):
        labels = service.attrs["Spec"]["Labels"]
        # Get all of the labels that start with the prefix,
        # but remove the prefix from the key
        prefixed_labels = filter_prefix_labels(labels, label_prefix)
        # Do the same with the Docker Flow labels
        prefixed_labels.update(filter_prefix_labels(labels, docker_flow_prefix))

        if "main" not in prefixed_labels:
            services.append(prefixed_labels)
        else:
            directory_service = prefixed_labels

    return render_template("index.html", directory_service=directory_service, services=services)


if __name__ == '__main__':
    app.run()
