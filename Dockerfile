FROM python:3-alpine
MAINTAINER James D. Marble <james.d.marble@gmail.com>

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt gunicorn
COPY . /usr/src/app

EXPOSE 80

CMD [ "gunicorn", "--bind=0.0.0.0:80", "app:app"]